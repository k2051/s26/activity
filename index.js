// Activity

const http = require('http')

// creates a variable 'port' to store port number
const port = 3000;

const server = http.createServer((request,response)=>{

	if(request.url == '/login'){
		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end("You are now in the login page")
	} else {
		response.writeHead(404,{'Content-Type':'text/plain'})
		response.end("Page not available")
	}
})
server.listen(port)
console.log(`server now accessible at localhost:${port}`)